// ==UserScript==
// @name         UTM Defiler
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Randomize query parameters in URLs with plausible values. Domains can be whitelisted, specific parameters can be white or blacklisted.
// @author       Alice Watson (https://lgbtqia.space/@alice)
// @source       https://codeberg.org/alicewatson/utm_defiler
// @match        *://*/*
// @grant        none
// @run-at       document-start
// @license      CC BY-NC 4.0
// ==/UserScript==

(function() {
    'use strict';

    const siteWhitelist = [
        'example.com', // Add the domains you want to whitelist here.
        // 'another-example.com',
    ];

    const paramBlacklist = [
        'q', // Add parameter names you DO NOT want to randomize here.
        // 'another-blacklisted-param',
    ];

    const specialParamWhitelist = {
        // 'special_param': ['value1', 'value2', ...],
    };

    function isWhitelisted(url) {
        return siteWhitelist.some(domain => url.includes(domain));
    }

    function scrambleValue(value) {
        return value.split('').sort(function() { return 0.5 - Math.random() }).join('');
    }

    function randomizeParameters(url) {
        const urlObj = new URL(url);
        const searchParams = urlObj.searchParams;

        // Replace all parameters with fake values, special whitelist values, or a scrambled string
        for (const [param, value] of searchParams) {
            if (specialParamWhitelist.hasOwnProperty(param)) {
                // Handle special whitelist parameters with a list of predefined values
                const values = specialParamWhitelist[param];
                searchParams.set(param, values[Math.floor(Math.random() * values.length)]);
            } else if (paramBlacklist.indexOf(param) === -1) { // Not in the blacklist
                if (param.startsWith('utm_')) {
                    // Handle known UTM parameters in a known way
                    searchParams.set(param, generateFakeUtmValue(param));
                } else {
                    // Scramble unknown parameter values
                    searchParams.set(param, scrambleValue(value));
                }
            }
            // If the parameter is in the blacklist, it will be ignored and remain unchanged
        }

        return urlObj.toString();
    }

    // Existing functions (including generateFakeUtmValue) go here...
    // ...

    function replaceUrlParameters() {
        const originalUrl = window.location.href;
        if (!isWhitelisted(originalUrl)) {
            const newUrl = randomizeParameters(originalUrl);
            if (newUrl !== originalUrl) {
                window.history.replaceState(null, '', newUrl);
            }
        }
    }

    replaceUrlParameters();
})();