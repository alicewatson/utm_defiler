# UTM Defiler

![An image of a little cat-demon surrounded by unplugged survailance equipment](utm_defiler_logo.jpg)

A [ViolentMonkey](https://violentmonkey.github.io/) script to randomize query parameters in URLs with plausible values. Domains can be whitelisted, specific parameters can be white or blacklisted.

Compatible with ViolentMonkey and probably TamperMonkey, GreaseMonkey, TheFunkyMonkey, etc.

This is a bare-bones script currently. No fancy UI or anything like that, but it does seem to work, so that's something.
